<?php
ini_set( 'memory_limit', '1000M' );
ini_set( 'max_execution_time', '6000' );
ini_set( 'max_input_time', '6000' );

error_reporting (E_ALL ^ E_NOTICE);
date_default_timezone_set('America/Sao_Paulo');


$CFG = new stdClass();

//connection to Sybase
$CFG->sb_dsn = "dblib:host=example.org:8000;dbname=XXX";
$CFG->sb_pass = "";
$CFG->sb_user = "";

$CFG->salt = 'algumstringbemcomplicado';

// try to no overload the loading of the local database
$CFG->maxrecords = 10000;
$CFG->sleeptime = 1; //seconds

