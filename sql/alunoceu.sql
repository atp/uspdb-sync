create table alunoceu (
    id int unsigned NOT NULL auto_increment,
    codpes int not null,
    dtacad datetime ,
    PRIMARY KEY(id),
    KEY `idx_codpes` (`codpes`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;
