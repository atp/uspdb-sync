create table atividadeceu (
    id int unsigned NOT NULL auto_increment,
    codund tinyint not null,
    codatvceu int not null,
    nomatvceu varchar(512) not null,
    dscatvceu longtext null,
    PRIMARY KEY(id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;
