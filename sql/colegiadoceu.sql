create table colegiadoceu (
    id int unsigned NOT NULL auto_increment,
    codclg smallint not null,
    sglclg char(12) not null,
    codundrsp tinyint not null,
    PRIMARY KEY(id),
    KEY `idx_codclg` (`codclg`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;
