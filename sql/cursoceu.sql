create table cursoceu (
    id int unsigned NOT NULL auto_increment,
    codund tinyint not null,
    codcurceu int not null,
    codsetdep smallint null,
    codmdlceu tinyint not null,
    codarecnh int null,
    nomcurceu varchar(512) not null,
    stareqgrd char(1) null,
    dscpbcinr varchar(512) not null,
    dtainc datetime not null,
    dtaalt datetime not null,
    codclg smallint null,
    sglclg char(12) null,
    fmtcurceu char(4) not null,
    PRIMARY KEY(id),
    KEY `idx_codcurceu` (`codcurceu`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;
