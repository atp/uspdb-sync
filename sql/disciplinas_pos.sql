CREATE TABLE disciplinas_pos (
    id int unsigned NOT NULL auto_increment,
    sgldis char(7) NOT NULL,
    numseqdis tinyint unsigned NOT NULL,
    dtaprpdis datetime, 
    nomdis varchar(1024),
    tipcurdis char(3),
    durdis tinyint unsigned, 
    cgahorteodis smallint,
    cgahoresddis smallint,
    cgahordis int,
    numcretotdis int,
    dtaatvdis datetime,
    dtadtvdis datetime,
    objdis text,
    jusdis text,
    tipavldis varchar(2048),
    ctudis text,
    ctubbgdis mediumtext,
    obsdis text,
    codare int,
    dtaaprcog datetime,
    PRIMARY KEY(id),
    KEY `idx_sgldis` (`sgldis`),
    KEY `idx_sgldis_numseqdis` (`sgldis`, `numseqdis`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC; 
