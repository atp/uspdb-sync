create table edicaocursoceu (
    id int unsigned NOT NULL auto_increment,
    codund tinyint not null,
    codcurceu int not null,
    codedicurceu smallint not null,
    dtainiedicur datetime not null,
    dtafimedicur datetime not null,
    staedi char(3) null,
    dtaaprund datetime null,
    dtaaprhomprr datetime null,
    dtaaprcshdep datetime null,
    dtaaprcshdlb datetime null,
    durcur decimal(5,2) null,
    tipdurcur char(1) null,
    PRIMARY KEY(id),
    KEY `idx_codcurceu` (`codcurceu`),
    KEY `idx_codedicurceu` (`codedicurceu`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;
