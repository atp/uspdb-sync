-- -----------------------------------------------------
-- Table `usp`.`emailpessoa`
-- -----------------------------------------------------
CREATE  TABLE `emailpessoa` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `codpes` INT UNSIGNED NOT NULL COMMENT 'Número USP' ,
  `codema` VARCHAR(80) NULL COMMENT 'Email' ,
  `numseqema` TINYINT NULL COMMENT 'Número sequencial do email da pessoa' ,
  `stamtr` CHAR(1) NULL COMMENT 'Para mandar mensagens sobre a matrícula' ,
  `staatnsen` CHAR(1) NULL COMMENT 'Para mandar senha' ,
  PRIMARY KEY (`id`),
  KEY `idx_emailpessoa_codema` (`codema`),
  KEY `idx_emailpessoa_codpes` (`codpes`),
  KEY `idx_emailpessoa_codpes_codema` (`codpes`,`codema`)
)
;
