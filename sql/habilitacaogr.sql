create table habilitacaogr (
id int unsigned NOT NULL auto_increment,
codcur int not null,
codhab smallint not null,
nomhab varchar(256) null,
tiphab char(1) not null,
perhab varchar(16) null,
PRIMARY KEY(id),
KEY `idx_codcur` (`codcur`),
KEY `idx_codhab` (`codhab`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;


