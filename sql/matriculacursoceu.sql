create table matriculacursoceu (
    id int unsigned NOT NULL auto_increment,
    codmtrcurceu int not null,
    codund tinyint not null,
    codcurceu int not null,
    codedicurceu smallint not null,
    codpes int not null,
    stacer tinyint null,
    stamtrcurceu char(3) default 'AND' not null,
    dtainiprj datetime null,
    dtafimprj datetime null,
    codmtrceuori int null,
    numseqofeedi tinyint not null,
    rstmtrcur char(3) null,
    PRIMARY KEY(id),
    KEY `idx_codmtrcurceu` (`codmtrcurceu`),
    KEY `idx_codpes` (`codpes`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;
