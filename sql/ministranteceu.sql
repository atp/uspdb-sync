create table ministranteceu (
    id int unsigned NOT NULL auto_increment,
    codofeatvceu int not null,
    codpes int not null,
    codatc tinyint null,
    cgahormis int default 0 not null,
    dtainimisatv datetime null,
    dtafimmisatv datetime null,
    fmtexeatv char(1) null,
    codatcrsd tinyint null,
    PRIMARY KEY(id),
    KEY `idx_codpes` (`codpes`),
    KEY `idx_codofeatvceu` (`codofeatvceu`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;
