create table modalidcursoceu (
    id int unsigned NOT NULL auto_increment,
    codmdlceu tinyint not null,
    dscmdlceu varchar(128) not null,
    cgahormin int not null,
    idfreqgrd char(1) not null,
    idftipatz char(1) not null,
    idfreqarecnh char(1) not null,
    PRIMARY KEY(id),
    KEY `idx_codmdlceu` (`codmdlceu`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;
