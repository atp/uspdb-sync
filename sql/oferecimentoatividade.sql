create table oferecimentoatividade (
    id int unsigned NOT NULL auto_increment,
    codofeatvceu int not null,
    codund tinyint not null,
    codcurceu int not null,
    codpgmceu int not null,
    codatvceu int not null,
    codedicurceu smallint null,
    qtdvagofe int not null,
    dtainiofeatv datetime null,
    dtafimofeatv datetime null,
    numseqofeedi tinyint null,
    PRIMARY KEY(id),
    KEY `idx_codofeatvceu` (`codofeatvceu`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;
