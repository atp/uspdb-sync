-- -----------------------------------------------------
-- Table `usp`.`pessoa`
-- -----------------------------------------------------
CREATE  TABLE `pessoa` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `codpes` INT UNSIGNED NOT NULL COMMENT 'Número USP' ,
  `nompes` VARCHAR(128) NULL COMMENT 'Nome completo' ,
  `numcpf` BINARY(32) NULL COMMENT 'Hash do CPF' ,
  `cpf` DECIMAL(11,0) NULL,
  `sexpes` CHAR(1) NULL COMMENT 'Sexo' ,
  `dtanas` DATE NULL COMMENT 'Data de nascimento' ,
  `tipdocidf` CHAR(6) ,
  `numdocidf` BINARY(32) NULL COMMENT 'Hash do documento',
  PRIMARY KEY (`id`),
  UNIQUE KEY `codpes_UNIQUE` (`codpes`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;
