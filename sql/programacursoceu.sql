create table programacursoceu (
    id int unsigned NOT NULL auto_increment,
    codund tinyint not null,
    codcurceu int not null,
    codpgmceu int not null,
    dtacripgmceu datetime not null,
    PRIMARY KEY (id),
    KEY `idx_codcurceu` (`codcurceu`),
    KEY `idx_codpgmceu` (`codpgmceu`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;
