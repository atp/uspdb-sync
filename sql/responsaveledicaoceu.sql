create table responsaveledicaoceu (
    id int unsigned NOT NULL auto_increment,
    codund tinyint not null,
    codcurceu int not null,
    codedicurceu smallint not null,
    numseqofeedi tinyint not null,
    codpes int not null,
    codpaprsp tinyint not null,
    tipvin char(25) null,
    tipcon char(10) null,
    tipjor char(10) null,
    sglund char(7) null,
    PRIMARY KEY(id),
    KEY `idx_codcurceu` (`codcurceu`),
    KEY `idx_codpes` (`codpes`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;
