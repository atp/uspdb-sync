
CREATE TABLE sync_times (
    name varchar(30) NOT NULL,
    sync date,
    PRIMARY KEY(name)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

