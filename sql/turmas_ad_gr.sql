CREATE TABLE turmas_ad_gr (
    id int unsigned NOT NULL auto_increment,
    coddis char(7) NOT NULL,
    verdis tinyint unsigned NOT NULL,
    codtur char(10) not null,
    dtainitur date,
    dtafimtur date,
    codpes int unsigned,
    PRIMARY KEY(id),
    KEY `idx_codpes` (`codpes`),
    KEY `idx_coddis` (`coddis`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;
