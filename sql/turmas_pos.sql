CREATE TABLE turmas_pos (
    id int unsigned NOT NULL auto_increment,
    sgldis char(7) NOT NULL,
    numseqdis tinyint unsigned NOT NULL,
    numofe smallint unsigned,
    dtainiofe date,
    dtafimofe date,
    codpes int unsigned,
    PRIMARY KEY(id),
    KEY `idx_codpes` (`codpes`),
    KEY `idx_sgldis` (`sgldis`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;
