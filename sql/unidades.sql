CREATE TABLE unidades (
    id int unsigned NOT NULL auto_increment,
-- id local
	codfusclgund smallint unsigned unique NOT NULL,
-- código da unidade / colegiado
	codoriclgund smallint unsigned NOT NULL,
-- código original da unidade / colegiado
    nomclgund varchar(256),
-- nome da unidade
	sglfusclgund varchar(64),
-- sigla da unidade
	clsgrpanr tinyint,
-- grupo da unidade (ensino/pesquisa, museu, serviço, administração...)
	grpsglanr varchar(128),
-- descrição do grupo
	nomloc varchar(256),
-- localidade (cidade)
	urlsie varchar(512),
-- URL da unidade
    PRIMARY KEY(id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

