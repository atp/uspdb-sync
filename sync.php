#!/usr/bin/php7.4
<?php

require('config.php');
require_once('UspTable.class.php');

// processando argumentos
$opts = getopt('d:f:cmlhvrs:');
$params = shift($opts);

if(! is_null($opts['h']) ){
    print_help();
    exit(0);
}

// verbose?
if(! is_null($opts['v']))
    $verbose = true;


// para testes!
if(! is_null($opts['s'])){
    $sql = $opts['s'];
    if(!$dicce = connect_dicce()) {
      print "Erro ao conectar com BD do DI";
      exit(1);
    }
    
    try {
        $res = $dicce->query($sql,PDO::FETCH_ASSOC);
        if($verbose){
            print "Dados baixados do DICCE.\n";
        }
        $rows = 0;
        foreach ($res as $row){
            if($rows == 0 ){
                $coln = 0;
                foreach ($row as $field=>$value){
                    if($coln == 0){
                        print "$field";
                    } else {
                        print "\t$field";
                    }
                    $coln += 1;
                }
                print("\n");
            }
            $coln = 0;
            foreach ($row as $field=>$value) {
                if($coln == 0){
                        print "$value";
                    } else {
                        print "\t$value";
                    }
                    $coln += 1;
            }
            print "\n";
            $rows += 1;
        }
    } catch (PDOException $e) {
        print "PDO error!: " . $e->getMessage();
    }
    exit(0);
}

// lista de tabelas que podemos sincronizar
if(! is_null($opts['f']) ){
    if(! file_exists($opts['f']) ){
        print "Arquivo {$opts['f']} não existe!\n\n";
        exit(1);
    } else
        include_once($opts['f']);
} else
    include_once('tables.php');

if(! is_null($opts['l']) ){
    print "\nTabelas que podem ser importadas:\n\n";
    foreach(array_keys($tables) as $t)
        print "\t$t\n";
    print "\n";
    exit(0);
}




// apagando tabela e substuir com nova, vazia, usando especifacao em sql/tabela.sql
if(! is_null($opts['r'])) {
    $recreate = true;
}

// escolhendo o BD
if(! is_null($opts['d'])) {
    if(! file_exists($opts['d']) ){
        print "Arquivo {$opts['d']} não existe!\n\n";
        exit(1);
    } else
        include_once($opts['d']);
} else
    include_once('config-local-dev.php');

if($verbose)
    print "Usando mysql database {$CFG->my_db_name} on {$CFG->my_db_host}.\n";



// recompor as tabelas síntese alunoturma_moodle e turmas_moodle
if(! is_null($opts['m'])) {
    if( ! $dblocal = connect_local()){
        print "Erro ao tentar conectar ao banco de dados local.\n";
        exit(1);
    }
    if($verbose)
        print "Conectado ao banco de dados local.\n";

    $tables = array('alunoturma_moodle', 'turmas_moodle');
    foreach($tables as $table){
        if($verbose)
            print "Atualizando tabela $table.\n\n";

        if($recreate){
            $query = "drop table if exists {$table}";
            if($verbose) {
                print "Dropping table {$table}\n\n";
            }
            
            if( ! $dblocal->query($query)){
                print("Erro dropping {$table}. \n".$dblocal->error."\n");
                exit(1);
            }
        }
        // criar as tabelas se não existem
        if(!$dblocal->query("select 1 from {$table}") && $dblocal->errno == 1146){
            print("Tabela {$table} nao existe, vamos criar\n\n");
            $fn = "sql/{$table}.sql";
            $sql = file_get_contents($fn);
            if($verbose) {
                print("creating {$table} from {$fn}\n\n");
                print($sql);
            }
            if(!$dblocal->query($sql)){
                print("Error creating $table:\n.{$dblocal->error}");
                exit(1);
            }
            
        }
        
        // aqui entra o trabalho de verdade
        
        // apagando dados antigos
        $dblocal->query("TRUNCATE TABLE {$table}");
        //$dblocal->query("delete from {$table}");
        if($verbose)
            print "Dados antigos da tabela {$table} apagados.\n";
	
        // tabela alunoturma
        if($table == 'alunoturma_moodle'){
            
            // disciplinas de graduação
            $sql_gr = "select * from alunoturma_gr";

            if($result = $dblocal->query($sql_gr)) {
                $records = array(); 
                foreach($result as $row)
                    $records[] = "(null, '{$row['coddis']}.{$row['verdis']}.{$row['codtur']}', {$row['codpes']}, 'GR','{$row['stamtr']}')";

                if(!empty($records)) {	  
                    $query = "insert into $table values " . implode(',',$records);
                    if( ! $dblocal->query($query)){
                        print("Erro na inserção de dados na tabela {$table}. \n".$dblocal->error."\n");
                        print("record: {$records[0]} \n");
                        exit(1);
                    }
                }
            } else {
                print "Tabela $table não foi atualizada corretamente.\nFalha ao importar disciplinas de graduação.\n\n";
                exit(1);
            }
            
            // disciplinas de pós
            $sql_pos = "select * from alunoturma_pos";

            if($result = $dblocal->query($sql_pos)){
                $records = array();
                foreach($result as $row) {
                    $st = $row['stamtrpgmofe'];
                    if($st == 'P'){
                        $status = 'I'; // 'P' (pré-matricula) é equivalente de I na graduação
                    } else {
                        $status = 'M'; // 'A' (aval do orientador) e 'D' (deferido) equivalente matriculado na graduação
                    }
                    $records[] = "(null, '{$row['sgldis']}.{$row['numseqdis']}.{$row['numofe']}', {$row['codpes']}, 'POS','{$status}')";
                }
                if(!empty($records)) {
                    $query = "insert into $table values " . implode(',',$records);
                    if( ! $dblocal->query($query)){
                        print("Erro na inserção de dados na tabela {$table}. \n".$dblocal->error."\n");
                        print("record: {$records[0]} \n");
                        exit(1);
                    }
                }

            } else {
                print "Tabela $table não foi atualizada corretamente.\nFalha ao importar disciplinas de pós.\n\n";
                exit(1);
            }

            if($verbose)
                print "Tabela $table atualizada corretamente.\n\n";
            
            set_last_sync($dblocal, $table);
        }

        // tabela turmas

        else if($table == 'turmas_moodle'){

            // turmas graduacao
            // $sql_gr = "SELECT coddis, verdis, codtur, codpes FROM turmas_gr";
            // Vamos fazer uma união das tabelas turmas_gr e turmas_ad_gr

            //$sql_gr = "select coddis,verdis,codtur,dtainitur, dtafimtur, codpes from (select t.coddis,t.verdis,t.codtur,t.dtainitur,t.dtafimtur,t.codpes from turmas_gr t union select a.coddis, a.verdis, a.codtur, a.dtainitur,a.dtafimtur,a.codpes from turmas_ad_gr a) u";
            $sql_gr = "select DISTINCT coddis,verdis,codtur,dtainitur, dtafimtur, codpes from
( select t.coddis,t.verdis,t.codtur,t.dtainitur,t.dtafimtur,t.codpes from turmas_gr t where t.codpes is not NULL
   union
  select a.coddis, a.verdis, a.codtur, a.dtainitur,a.dtafimtur,a.codpes from turmas_ad_gr a where a.codpes is not NULL
 ) u
";
            if($result = $dblocal->query($sql_gr)){
                $records = array();
                foreach($result as $row) {
                    //$codpes = (!empty($row['codpes'])) ? $row['codpes'] : 0;
		    //records[] = "(null, '{$row['coddis']}.{$row['verdis']}.{$row['codtur']}', {$codpes}, '{$row['dtainitur']}','{$row['dtafimtur']}', 'GR')";
		    if  (!empty($row['codpes']) && !is_null($row['codpes'])){
			    $records[] = "(null, '{$row['coddis']}.{$row['verdis']}.{$row['codtur']}', {$row['codpes']}, '{$row['dtainitur']}','{$row['dtafimtur']}', 'GR')";
		    }
		}
		// @atp - debug
		echo "union ALL not null com ad_gr ";
		var_dump(count($records));
		//print_r($records);
		//echo "=====================================================================================================";
	    
		/*
                // Agora acrescentamos as linhas da tabela turmas_resp_gr, desde que não já estão no união acima
                //$sql_gr = "select coddis,verdis,codtur, r.dtainitur,r.dtafimtur,r.codpes from (select * from turmas_gr t union select * from turmas_ad_gr a) u right outer join turmas_resp_gr as r using(coddis,verdis,codtur) where u.id is null";

	        $sql_gr = "select DISTINCT u.coddis,u.verdis,u.codtur, r.dtainitur,r.dtafimtur,r.codpes from
(
  select * from turmas_gr t  where t.codpes is not NULL
   union
  select * from turmas_ad_gr a where a.codpes is not NULL
) u
  inner join turmas_resp_gr as r
 on u.coddis=r.coddis 
    AND u.verdis=r.verdis
    AND u.codtur=r.codtur
  where  r.codpes is not NULL
    AND  u.coddis is not NULL
    AND  u.verdis is not NULL
    AND  u.codtur is not NULL
";

            if($result = $dblocal->query($sql_gr)){
                foreach($result as $row) {
		    if  (!empty($row['codpes']) && !is_null($row['codpes'])){
			    $records[] = "(null, '{$row['coddis']}.{$row['verdis']}.{$row['codtur']}', {$row['codpes']}, '{$row['dtainitur']}','{$row['dtafimtur']}', 'GR')";
		    }
		}
		// @atp - debug
		echo "Union ALL not null com ad_gr right join resp_gr ";
		var_dump(count($records));
		//print_r($records);
		//echo "=====================================================================================================";
	    }

                $sql_gr = " select DISTINCT u.coddis
  ,u.verdis
  ,u.codtur
  , IF(ISNULL(u.dtainitur),r.dtainitur,u.dtainitur) as dtainitur
  , IF(ISNULL(u.dtafimtur),r.dtafimtur,u.dtafimtur) as dtafimtur
  , IF(ISNULL(u.codpes), r.codpes,u.codpes) as codpes
  from
(
  select * from turmas_gr t where t.codpes is NULL
) u
  inner join turmas_resp_gr as r
 using(coddis,verdis,codtur)
--  where  r.codpes is not NULL
";

                if($result = $dblocal->query($sql_gr)){
                    foreach($result as $row) {
                        $codpes = (!empty($row['codpes'])) ? $row['codpes'] : 0;
                        $records[] = "(null, '{$row['coddis']}.{$row['verdis']}.{$row['codtur']}', {$codpes}, '{$row['dtainitur']}','{$row['dtafimtur']}', 'GR')";
                    }
		// @atp - debug
		    echo "NULL usando resp_gr quando possivel ";
		    var_dump(count($records));
		    //print_r($records);
		    //echo "=====================================================================================================";
		}
		 */

		$sql_gr = "select DISTINCT uu.coddis
  ,uu.verdis
  ,uu.codtur
  , IF(ISNULL(uu.dtainitur),rr.dtainitur,uu.dtainitur) as dtainitur
  , IF(ISNULL(uu.dtafimtur),rr.dtafimtur,uu.dtafimtur) as dtafimtur
  , IF(ISNULL(uu.codpes), rr.codpes,uu.codpes) as codpes
  from
(select DISTINCT coddis
  ,verdis
  ,codtur
  , IF(ISNULL(u.dtainitur),r.dtainitur,u.dtainitur) as dtainitur
  , IF(ISNULL(u.dtafimtur),r.dtafimtur,u.dtafimtur) as dtafimtur
  , IF(ISNULL(u.codpes), r.codpes,u.codpes) as codpes
  from
(
  select * from turmas_gr t where t.codpes is NULL
) u
  left join turmas_ad_gr as r
 using(coddis,verdis,codtur) ) uu
 left join turmas_resp_gr as rr
 using(coddis,verdis,codtur)
";

                if($result = $dblocal->query($sql_gr)){
		    foreach($result as $row) {
			$codpes = (!empty($row['codpes'])) ? $row['codpes'] : 0;
			$codturSanitizado = $row['codtur'];
			if(strpos($row['codtur'],'\'') != false){
				$codturSanitizado = str_replace('\'','',$row['codtur']);
			}
			$records[] = "(null, '{$row['coddis']}.{$row['verdis']}.{$codturSanitizado}', {$codpes}, '{$row['dtainitur']}','{$row['dtafimtur']}', 'GR')";
                    }
		// @atp - debug
		    echo "NULL right join ad_jr ";
		    var_dump(count($records));
		    //print_r($records);
		    //echo "=====================================================================================================";
                }
                
                if(!empty($records)) {
		    $recordsSemDuplicidade = array_unique($records);
			/*
		    print("Excluimos duplicidades");
		    print_r($recordsSemDuplicidade);
		    print(var_dump(count($recordsSemDuplicidade))." registros sem duplicidade \n" );
			 */
		    $query = "insert into $table values " . implode(',',$recordsSemDuplicidade);
		    //print_r($query);
                    if( ! $dblocal->query($query)){
                        print("Erro na inserção de dados na tabela {$table}. \n".$dblocal->error."\nErrorcode: ".$dblocal->errno."\n");
			print_r($dblocal->error_list);
                        print("record: {$records[0]} \n");
                        exit(1);
                    }
		    print(var_dump(count($recordsSemDuplicidade))." inseridos em turmas_moodle" );
                }
            } else {
                print "Tabela $table não foi atualizada corretamente.\nFalha ao importar turmas de graduação:\n\n" .$dblocal->error. "\n";
                exit(1);
            }

            // turmas pos
            $sql_pos = "SELECT sgldis, numseqdis, numofe, dtainiofe, dtafimofe, codpes FROM turmas_pos ";
            if($result = $dblocal->query($sql_pos)){
                $records = array();
                foreach ($result as $row){
		    $codpes = (!empty($row['codpes'])) ? $row['codpes'] : 0;
		    $records[] = "(null, '{$row['sgldis']}.{$row['numseqdis']}.{$row['numofe']}', {$codpes}, '{$row['dtainiofe']}','{$row['dtafimofe']}' , 'POS')";
		}
		// @atp - debug
		echo "POS-G ";
		var_dump(count($records));
		//print_r($records);

                if(!empty($records)) {
                    $query = "insert into $table values " . implode(',',$records);
                    if( ! $dblocal->query($query)){
                        print("Erro na inserção de dados na tabela {$table}. \n".$dblocal->error."\n");
                        print("record: {$records[0]} \n");
                        exit(1);
                    }
                }
            } else {
                print "Tabela $table não foi atualizada corretamente.\nFalha ao importar turmas de pós.\n\n";
                exit(1);
            }

            if($verbose)
                print "Tabela $table atualizada corretamente.\n\n";
            
            set_last_sync($dblocal, $table);
        }
    }
    $dblocal->close();
    print("Re-feitas as tabelas alunoturma_moodle e turmas_moodle\n");
    exit(0);
}

if(! is_null($opts['c'])) {
    if( ! $dblocal = connect_local()){
        print "Erro ao tentar conectar ao banco de dados local.\n";
        exit(1);
    }
    if($verbose)
        print "Conectado ao banco de dados local.\n";
    // cohorts_moodle
    
    $sql_drop = "drop table if exists cohorts_moodle";
    $sql_cohorts = "create table cohorts_moodle as select codpes, anosem, s.codclg, s.sglclg, staalu, s.codcur, vinpgm, perhab, nomcur, u.codund,u.tipund,u.sglund,u.nomund from sitalunoativogr s join cursogr c on s.codclg = c.codclg and s.sglclg = c.sglclg and s.codcur = c.codcur join unidcoleg uc on s.codclg = uc.codclg and s.sglclg = uc.sglclg join unidade u on uc.codund = u.codund";
    if($dblocal->query($sql_drop)){
        if($dblocal->query($sql_cohorts)) {
            set_last_sync($dblocal, "cohorts_moodle");
        } else {
            print("Problema com SQL que cria o cohorts_moodle\n".$dblocal->error."\n");
            exit(1);
        }
    } else {
        print("Problema com SQL que cria o cohorts_moodle\n".$dblocal->error."\n");
        exit(1);
    }
    $dblocal->close();
    print("Re-feita a tabela cohorts_moodle\n");
    exit(0);
}

// se 'all' é um dos parâmetros, atualiza todas as tabelas
if(in_array('all', $params))
    $params = array_keys($tables);


if(count($params) == 0){
    print "Argumento requerido (TABELA) faltando.\n";
    print_help();
    exit(1);
}

if( ! $dicce = connect_dicce() ){
    print "Erro ao tentar conectar ao Sybase corporativo.\n";
    exit(1);
}
if($verbose)
    print "Conectado ao Sybase corporativo.\n";


// processando as tabelas
foreach($params as $table){
    // conectando aos BDs

    if( ! $dblocal = connect_local()){
        print "Erro ao tentar conectar ao banco de dados local.\n";
        exit(1);
    }
    if($verbose)
        print "Conectado ao banco de dados local.\n";
    if(array_key_exists($table, $tables)){

        if($recreate){
            if($verbose){
                print("Re-criando tabela $table\n");
            }
            if($tables[$table]->recreate($dblocal)){
                print "Tabela $table re-criada.\n\n";
            } else
                print "Tabela $table não foi re-criada corretamente.\n\n";
        }        
        // aqui entra o trabalho de verdade
        if($tables[$table]->process($dblocal, $dicce)){
            if($verbose)
                print "Tabela $table atualizada corretamente.\n\n";
        } else
            print "Tabela $table não foi atualizada corretamente.\n\n";
        
    } else
        print "Tabela $table não existe!\n\n";
    $dblocal->close();
}

if($verbose)
  print "Feito";



/**
 * conecta a um banco de dados mysql
 *
 * recebe uma lista com as informações de login para o banco de dados
 *
 * retorna o link resource, ou false em caso de erro.
 */
function connect_local(){
    global $CFG;
    // conectando a base de dados local
    $mysqli = new mysqli($CFG->my_db_host,$CFG->my_db_user,$CFG->my_db_password,$CFG->my_db_name);
    if (!$mysqli) {
        $err= "CONNECT FAIL: " 	. mysqli_connect_errno() . ' '. mysqli_connect_error() ;
        trigger_error($err, E_USER_ERROR);
        return false;
    }
    return $mysqli;
}

/**
 * conecta ao BD do CCE
 *
 * retorna o resource da conexão, ou false em caso de erro
 */
function connect_dicce(){
    global $CFG;
    $options = array(
        'debug'       => 2,
        PDO::ATTR_TIMEOUT => 300,
    );

    try {
        $db = new PDO($CFG->sb_dsn,$CFG->sb_user,$CFG->sb_pass, $options);
        return $db;
    } catch (PDOException $e) {
        print('Nao conectou ao DICCE: '.$e->getMessage());
        return false;
    }
}

function set_last_sync($dblocal,$tablename){
    $time = date('Y-m-d H:i:s');
    $sql = "REPLACE INTO sync_times VALUE('{$tablename}', '$time')";
    
    if(! $dblocal->query($sql)) {
        print("Problema atualizar sync_times");
        exit(1);
    }
}

/**
 * limpa o ARGV das opções já processadas
 *
 * recebe como parâmetro a lista de opções retornada pelo getopt
 *
 * retorna a lista de parâmetros restantes
 */
function shift($options_array)
{
    foreach( $options_array as $o => $a )
    {
        // Look for all occurrences of option in argv and remove if found :
        // ----------------------------------------------------------------
        // Look for occurrences of -o (simple option with no value) or -o<val> (no space in between):
        while($k=array_search("-".$o.$a,$GLOBALS['argv']))
        {    // If found remove from argv:
            if($k)
                unset($GLOBALS['argv'][$k]);
        }
        // Look for remaining occurrences of -o <val> (space in between):
        while($k=array_search("-".$o,$GLOBALS['argv']))
        {    // If found remove both option and value from argv:
            if($k)
            {    unset($GLOBALS['argv'][$k]);
            unset($GLOBALS['argv'][$k+1]);
            }
        }
    }
    // Reindex :
    $GLOBALS['argv']=array_merge($GLOBALS['argv']);

    return array_slice($GLOBALS['argv'], 1);
}

/**
 * imprime texto de ajuda do script
 */
function print_help(){
    $myself = $GLOBALS['argv'][0];

    print<<<END
USO:\n\t$myself [ -f FILE -d FILE]  TABELA
    
TABELA           "all" ou lista de tabelas a serem atualizadas

Opções:
    -l           lista as tabelas disponíveis
    -m           recompor as tabelas alunoturma_moodle e turmas_moodle
    -r           apaga todo conteúdo da tabela e re-cria nova tabela vazia
    -d FILE      arquivo de configuração mysql local
    -f FILE      usa tabelas do arquivo FILE
    -s SQL       roda SQL no DB do DICCE e mostra os resultados, para fins de teste
    -h           mostra esta tela de ajuda
    -v           modo verbose


END;

}
